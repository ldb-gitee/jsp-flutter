import 'package:flutter/material.dart';
import 'package:jsp_flutter/pages/clip_demo.dart';
import 'package:jsp_flutter/pages/draggable_demo.dart';
import 'package:jsp_flutter/pages/expansion_panel_list.dart';
import 'package:jsp_flutter/pages/expansion_tile.dart';
import 'package:jsp_flutter/pages/frosted_glass.dart';
import 'package:jsp_flutter/pages/keep_alive.dart' as jsp_ka;
import 'package:jsp_flutter/pages/pages.dart';
import 'package:jsp_flutter/pages/right_back.dart';
import 'package:jsp_flutter/pages/search_bar.dart';
import 'package:jsp_flutter/pages/splash_screen.dart';
import 'package:jsp_flutter/pages/wrap_demo.dart';
import 'package:jsp_flutter/product.dart';
import 'package:jsp_flutter/product_list.dart';
import 'bottom_navigation_widget.dart';
import 'bottom_fab_navigation_widget.dart';
import 'package:flutter/rendering.dart';

void main() {
  //debugPaintSizeEnabled = true;
  runApp(MaterialApp(title: 'jsp flutter examples', home: new SplashScreen()));
}


class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(title: Text('首页')),
        body: Center(
            child: 
              GridView.count(
                crossAxisCount: 3,
                childAspectRatio: 2.0,
                children: <Widget>[
                  Container(child: Image.asset('images/flutter_logo.png'),),
                  RaisedButton(
                    child: Text('商品列表'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new ProductList(
                                products: List.generate(
                                  20, 
                                  (index) => Product('商品$index', '这是商品详情，编号为$index')
                                  ),
                              )
                          )
                          );
                    },
                  ),
                  RaisedButton(
                    child: Text('返回控制'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new ProductList(
                                products: List.generate(
                                  20, 
                                  (index) => Product('商品$index', '这是商品详情，编号为$index')
                                  ),
                              )
                          )
                          );
                    },
                  ),
                  RaisedButton(
                    child: Text('底部导航'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new BottomNavigationWidget() //BottomNavigationExample()
                          )
                          );
                    },
                  ),
                  RaisedButton(
                    child: Text('不规则底部导航'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new BottomFabNavigationWidget()
                          )
                          );
                    },
                  ),
                  RaisedButton(
                    child: Text('路由动画'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new FirstPage()
                          )
                          );
                    },
                  ),
                  RaisedButton(
                    child: Text('毛玻璃'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new FrostedGlass()
                          )
                          );
                    },
                  ),
                  RaisedButton(
                    child: Text('保持状态'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new jsp_ka.KeepAlive()
                          )
                          );
                    },
                  ),
                  RaisedButton(
                    child: Text('搜索条'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new SearchBar()
                          )
                          );
                    },
                  ),
                  RaisedButton(
                    child: Text('wrap流式布局'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new WrapDemo()
                          )
                          );
                    },
                  ),
                  RaisedButton(
                    child: Text('展开闭合'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new ExpansionTileDemo()
                          )
                          );
                    },
                  ),
                  RaisedButton(
                    child: Text('展开闭合列表'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new ExpansionPanelListDemo()
                          )
                          );
                    },
                  ),
                  RaisedButton(
                    child: Text('贝塞尔曲线'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new BottomClipperDemo()
                          )
                          );
                    },
                  ),
                  RaisedButton(
                    child: Text('滑动返回'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new RightBackDemo()
                          )
                          );
                    },
                  ),
                  Tooltip(
                    message: '我是提示信息',
                    child: RaisedButton(
                      child: Text('长按提示'),
                      onPressed: () {
                        
                      },
                    ),
                  ),
                  RaisedButton(
                    child: Text('拖拽'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new DraggableDemo()
                          )
                          );
                    },
                  ),
                ],
              )
          ));
  }
}


