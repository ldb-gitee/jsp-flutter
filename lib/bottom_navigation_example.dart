import 'package:flutter/material.dart';
import 'bottom_navigation_widget.dart';

class BottomNavigationExample extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter bottomNavigationBar',
      theme: ThemeData.light(),
      home: new BottomNavigationWidget(),
    );
  }
}