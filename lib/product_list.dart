
import 'package:flutter/material.dart';

import 'product.dart';

class ProductList extends StatelessWidget {
  final List<Product> products;
  ProductList({Key key, @required this.products}) : super(key: key);

   @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('商品列表'),),
      body: ListView.builder(
        itemCount: products.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(products[index].title),
            onTap: () {
              _navigateToDetailPage(context, products[index]);

              // Navigator.of(context).push( 
              //   MaterialPageRoute(
              //     builder: (context) => new ProductDetail(product: products[index])
              //   )
              // ).then((value) {
              //     print(value);
              //     Scaffold.of(context).showSnackBar(SnackBar(content: Text('$value'),));
              //   });
            },
          );
        },
      ),
    );
  }

  _navigateToDetailPage(BuildContext context, Product product) async {
    final result = await Navigator.push(
      context, MaterialPageRoute(
        builder: (context) => new ProductDetail(product: product,)
        )
    );
    Scaffold.of(context).showSnackBar(SnackBar(content: Text('结果：$result'),));
  }
}

class ProductDetail extends StatelessWidget {
  final Product product;
  ProductDetail({Key key, @required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(product.title),
        automaticallyImplyLeading: false,
      ),
      body: Center(
          child: Text(product.description),
        ),
      floatingActionButton: FloatingActionButton(
          child: Text('返回'),
          onPressed: () {
            Navigator.pop(context, '返回${product.title}');
          },
        ),
    );
  }
}
